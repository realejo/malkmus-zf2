<?php
namespace MkUser\Entity;

use ZfcUser\Entity\User as ZfcEntityUser;

class User extends ZfcEntityUser implements \ArrayAccess
{

    /**
     *
     * @var int
     */
    protected $root;

    /**
     *
     * @var int
     */
    protected $beta;

    /**
     *
     * @var int
     */
    protected $active;

    /**
     *
     * @return int
     */
    public function getBeta()
    {
        return $this->beta;
    }

    /**
     *
     * @return int
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     *
     * @param int $root
     */
    public function setRoot($root)
    {
        $this->root = $root;
    }

    /**
     *
     * @param int $beta
     */
    public function setBeta($beta)
    {
        $this->beta = $beta;
    }

    /**
     *
     * @param int $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * Apenas para não quebrar o site.
     * Tem que remover em algum momento
     */
    public function offsetSet($offset, $value)
    {
        $offset = $this->_mapField($offset);
        $this->$offset = $value;
    }

    public function offsetExists($offset)
    {
        $offset = $this->_mapField($offset);
        return property_exists($this, $offset);
    }

    public function offsetUnset($offset)
    {
        $offset = $this->_mapField($offset);
        throw new \Exception("Cannot remove the property $offset");
    }

    public function offsetGet($offset)
    {
        $offset = $this->_mapField($offset);
        if ($this->offsetExists($offset)) {
            return $this->$offset;
        }

        throw new \Exception("$offset property does not exists");
    }

    private function _mapField($keyFrom)
    {
        if ($keyFrom === 'id_user') {
            return 'id';
        } elseif ($keyFrom === 'name') {
            return 'displayName';
        }

        return $keyFrom;
    }
}

