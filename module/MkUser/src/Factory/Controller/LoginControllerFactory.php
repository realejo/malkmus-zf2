<?php
namespace MkUser\Factory\Controller;

use Zend\Mvc\Controller\ControllerManager;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use MkUser\Controller\UserController;

class LoginControllerFactory implements FactoryInterface
{
    /**
     * Create controller
     *
     * @param ControllerManager $serviceLocator
     * @return UserController
     */
    public function createService(ServiceLocatorInterface $controllerManager)
    {
        /* @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $controllerManager->getServiceLocator();

        $userService = $serviceLocator->get('mkuser_user_service');
        $userForm = $serviceLocator->get('mkuser_user_form');
        $options = $serviceLocator->get('mkuser_module_options');

        $controller = new LoginController($userService, $options, $userForm);

        return $controller;
    }
}
