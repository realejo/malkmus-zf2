<?php
/**
 * plugin para gerenciar a inclusão de JS e CSS dos vendors
 *
 * @copyright Copyright (c) 2013 Realejo (http://realejo.com.br)
 */
namespace MkUser\Model;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class Vendor implements ServiceLocatorAwareInterface
{
    /**
     * Service Locator, tem que substituir o Loader por ele
     * @var ServiceLocatorInterface
     */
    protected $services;

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->services = $serviceLocator;
    }

    public function getServiceLocator()
    {
        return $this->services;
    }

    /**
     * Inicializa o alerta da Realejo
     */
    public function jsCursoInscritos()
    {
        $this->headScript()->appendFile('/js/curso-inscritos-2.0.js');

        // Mantem a cadeia
        return $this;
    }

    /**
     * Inicializa o alerta da Realejo
     */
    public function jsAlert()
    {
    	$this->headScript()->appendFile('/js/rw.alerts-0.3.js');

    	// Mantem a cadeia
    	return $this;
    }

    /**
     * Inicializa o alerta da Realejo
     */
    public function jsVideo()
    {
        $this->headScript()->appendFile('/js/video-view-1.0.js');

        // Mantem a cadeia
        return $this;
    }

    /**
     * Inicializa o Filestyle com Boostrap
     */
    public function jsBoostrapFilestyle()
    {
        // @todo (deprecated) remover referencias em controllers.
        // ele foi inserido no layout.

        // Mantem a cadeia
        return $this;
    }

    public function jsMidiaPlayer()
    {
        $this->headScript()->appendFile('/js/vendor/mediaelement-and-player-2.13.1.min.js');
        $this->headLink()->appendStylesheet("/css/vendor/mediaelementplayer-2.13.1.min.css");
    }

    /**
     * Inicializa o chosen
     */
    public function jsChosen()
    {
        // Coloca o script na view
        $this->headScript()->appendFile('/js/vendor/jquery.chosen-0.9.12.js');
        $this->headLink()->appendStylesheet("/css/vendor/jquery.chosen-0.9.12.css");

        // Mantem a cadeia
        return $this;
    }

    /**
     * Inicializa o masked input
     * @todo iremos usar esse ou o maskedinput?
     */
    public function jsMask()
    {
        $this->headScript()->appendFile('/vendor/jquery-mask-plugin-1.7.4/jquery.mask.js');

        // Mantem a cadeia
        return $this;
    }

    /**
     * Bootrap validator
     */
    public function jsValidate()
    {
    	$this->headScript()->appendFile('/vendor/formvalidation-dist-v0.6.3/dist/js/formValidation.min.js');
    	$this->headScript()->appendFile('/vendor/formvalidation-dist-v0.6.3/dist/js/framework/bootstrap.min.js');
    	$this->headScript()->appendFile('/vendor/formvalidation-dist-v0.6.3/dist/js/language/pt_BR.js');
    	$this->headLink()->appendStylesheet('/vendor/formvalidation-dist-v0.6.3/dist/css/formValidation.min.css');

        // Mantem a cadeia
        return $this;
    }

    public function jsSlug()
    {
        $this->headScript()->appendFile('/vendor/jquery-seourl-0.2/rw.seourl-0.2.js');

        // Mantem a cadeia
        return $this;
    }

    public function jsJQueryUI()
    {
        $this->headScript()->appendFile('/vendor/jquery-ui-1.11.4.custom/jquery-ui.min.js');
        $this->headScript()->appendFile('/vendor/jquery-ui-1.11.4.custom/datepicker-pt-BR.js');
        $this->headLink()->appendStylesheet("/vendor/jquery-ui-1.11.4.custom/jquery-ui.css");
        $this->headLink()->appendStylesheet("/vendor/jquery-ui-1.11.4.custom/jquery-ui.theme.css");
        // Mantem a cadeia
        return $this;
    }

    public function jsColorBox()
    {
        $this->headScript()->appendFile('/js/vendor/jquery.colorbox-1.3.20-min.js');
        $this->headLink()->appendStylesheet('/css/vendor/colorbox-1.0.0.css');

        // Mantem a cadeia
        return $this;
    }

    public function jsSelect2()
    {
        $this->headScript()->appendFile('/vendor/select2-3.5.1/select2.min.js');
        $this->headLink()->appendStylesheet('/vendor/select2-3.5.1/select2.css');
        // Mantem a cadeia
        return $this;
    }

    public function jsMediaElementPlayer()
    {
        $this->headScript()->appendFile('/vendor/mediaelement-and-player-2.16.1/mediaelement-and-player.min.js');
        $this->headLink()->appendStylesheet('/vendor/mediaelement-and-player-2.16.1/mediaelementplayer.min.css');

        // Mantem a cadeia
        return $this;
    }

    public function jsFlowPlayer()
    {
        $this->headScript()->appendFile('/vendor/flowplayer-3.2.4/flowplayer-3.2.4.min.js');

        // Mantem a cadeia
        return $this;
    }

    public function headScript()
    {
        return $this->getServiceLocator()
                    ->get('viewhelpermanager')
                    ->get('HeadScript');
    }

    public function headLink()
    {
        return $this->getServiceLocator()
                    ->get('viewhelpermanager')
                    ->get('HeadLink');
    }
}
