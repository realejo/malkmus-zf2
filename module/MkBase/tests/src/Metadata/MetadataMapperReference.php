<?php
/**
 * Classe mapper para ser usada nos testes
 */
namespace MkBaseTest\Metadata;

use MkBase\Mapper\MapperAbstract;

class MetadataMapperReference extends MapperAbstract
{
    protected $tableName = 'tblreference';
    protected $tableKey  = 'id_reference';
}
