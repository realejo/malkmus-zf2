#!/bin/bash
if [ "$1" == "semaphore" ]; then
	phpunit -c phpunit.semaphore.xml --stop-on-error $2
	exit;
fi;

phpunit $1 $2
