<?php
// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'testing'));

// Define application environment
defined('TEST_ROOT')
    || define('TEST_ROOT', realpath(dirname(__FILE__)));

// Define application environment
defined('APPLICATION_DATA')
    || define('APPLICATION_DATA', realpath(dirname(__FILE__) . '/assets/data'));

// Carrega o autoloader do composer
$loader = require_once realpath(dirname(__FILE__) . '/../../../vendor') . '/autoload.php';

// Carrega os namespaces para teste
$loader->addPsr4("MkStdlibTest\\", __DIR__ . "/src");
