<?php
namespace MkUser\Controller;

use MkUser\Model\Controller\ActionController;
use Zend\View\Model\ViewModel;
use Usuario\Model\Usuario;
use Realejo\Image;

class UserController extends ActionController
{
    /**
     * Verifica se tem acesso
     */
    public function onDispatch(\Zend\Mvc\MvcEvent $e)
    {
        // Verifica se tem permissão de estar aqui
        if (!$this->getAccessControl()->hasIdentity()) {
            return $this->redirect()->toUrl('/user/login');
        }

        return parent::onDispatch($e);
    }

    /**
     * Action para alterar o user
     */
    public function indexAction()
    {
        // Recupera o usuário
        $idu = $this->params('idu');
        if (empty($idu)) {
            return $this->redirect()->toRoute('/users');
        }

        // Verifica se o usuário existe
        $user = $this->getUserService()->fetchRow($idu);
        if (empty($user)) {
            return $this->redirect()->toRoute('/users');
        }

        $this->getServiceLocator()->get('ViewHelperManager')->get('HeadTitle')->set('Informações do usuário');

        // Configura o view
        return array(
            'user' => $user
        );
    }

    /**
     * Action para alterar o user
     */
    public function personalAction()
    {
        // Recupera o usuário
        $idu = $this->params('idu');
        if (empty($idu)) {
            return $this->redirect()->toRoute('users');
        }

        // Verifica se o usuário existe
        //$user = $this->getUserService()->setUseCache(false)->fetchRow($idu);
        $user = $this->getUserService()->getUserMapper()->findById($idu);
        if (empty($user)) {
            return $this->redirect()->toRoute('users');
        }

        // Verifica se tem acesso ao usuário
        $canUpdate = true;

        // Verifica se é o envio do formulário
        if ($this->getRequest()->isPost() === false) {

            if (!$this->isAjax()) {
                $this->getVendor()->jsChosen()->jsValidate();
                $this->getServiceLocator()->get('ViewHelperManager')->get('HeadTitle')->set('Informações do usuário');
            }

            // Configura o view
            return array(
                'user'   => $user,
                'canUpdate' => $canUpdate,
            );
        }

        $setUpdate = array(
            'email'  => $this->getRequest()->getPost('email', $user['email']),
            'name'   => $this->getRequest()->getPost('name', $user['name']),
            'active' => $this->getRequest()->getPost('active', 0),
            'root'   => $this->getRequest()->getPost('root', 0),
            'beta'   => $this->getRequest()->getPost('beta', 0),
        );

        // Somente o user pode criar user e definir beta
        if (!$this->getAccessControl()->isRoot()) {
            unset($setUpdate['user']);
            if (!$this->getAccessControl()->isBeta()) {
                unset($setUpdate['beta']);
            }
        }

        // Verifica se as passwords são iguais
        $password = $this->getRequest()->getPost('password');
        $password1 = $this->getRequest()->getPost('password1');

        // Verifica se foi preenchido alguma password
        // Só altera se foi preenchido
        if (!empty($password) || !empty($password)) {
            if ( $password === $password1 ) {
                $setUpdate['password'] = $password;
            } else {
                // Grava a mensagem ao usuário
                $this->userMessenger()->addErrorMessage("As senhas não são iguais.");

                // retorna para a página de usuários
                return $this->redirect()->toRoute(
                    'users/user',
                    array('idu' => $idu),
                    array('fragment' => 'personal')
                );
            }
        }

        $setUpdate['blame'] = $this->getAccessControl()->getUser()->id_user;

        // Grava as alterações
        $this->getUserService()->update($setUpdate, $idu);

        // Grava a mensagem ao usuário
        $this->userMessenger()->addSuccessMessage("Usuário <b>{$user['name']}</b> salvo.");

        // retorna para a página de usuários
        return $this->redirect()->toRoute('users');
    }

    public function permissionAction()
    {
        // Recupera o usuário
        $idu = $this->params('idu');
        if (empty($idu)) {
            return $this->redirect()->toRoute('users');
        }

        // Verifica se o usuário existe
        $user = $this->getUserService()->fetchRow($idu);
        if (empty($user)) {
            return $this->redirect()->toRoute('users');
        }

        return;

        // Verifica se tem acesso ao usuário
        $canUpdate = $this->_canUpdate($user);

        // O user não pode alterar seus role
        if ($this->getAccessControl()->isSame($user)) $isUpdate = false;

        // Verifica se é alteração
        $isUpdate = ($canUpdate) ? $this->params('update', false) : false;

        // Verifica se houve POST
        if ($isUpdate && $this->getRequest()->isPost()) {
            $dados = array();

            // Grava as permissões
            $roles = $this->getRequest()->getPost('roles');
            $this->getUserService()->getAcl()->setRoles($idu, $roles);

            // Grava a mensagem ao usuário
            $this->userMessenger()->addSuccessMessage("Permissões do usuário <b>{$user['nome']}</b> salvas.");

            // Retorna para a página de usuários
            return $this->redirect()->toUrl('/users/info/'.$idu);

        }// Fim do post

        // Recupera os modulos disponíveis
        $modulos = $this->getAccessControl()->getAclBffc()->getModulos();
        //RW_Debug::dump($modulos,'$modulos');

        // Recupera os roles que o usuário possui
        $roles = $this->getUserService()->getAcl()->getRoles($idu);
        $userRoles = array();
        if (!is_null($roles)) {
            foreach ($roles as $r) {
                $userRoles[$r['modulo']] = $r['role'];
            }
        }

        // Recupera os roles do usuário tem acesso (o que está logado)
        $roles = $this->getUserService()->getAcl()->getRoles($this->getAccessControl()->getUser()->id_user);
        $superRoles = array();
        if (!is_null($roles)) {
            foreach ($roles as $r) {
                $superRoles[$r['modulo']] = $r['role'];
            }
        }
        //RW_Debug::dump($superRoles,'$superRoles');

        // Recepera os roles que o usuário logado tem acesso
        $roles = array();
        if ($isUpdate) {
            foreach ($modulos as $m=>$modulo) {
                foreach ($modulo['roles'] as $r=>$role) {
                    if ($this->getAccessControl()->isRoot()
                            || ($this->getAccessControl()->isFranqueado() && $isPDVUser)
                            || (!$isPDVUser && !empty($superRoles[$m]) && ($superRoles[$m] === $r || $this->getAccessControl()->acl->inheritsRole($m, $superRoles[$m], $r)) )
                            || ($isPDVUser  && !empty($superRoles[$m]) && ($superRoles[$m] === $r || $this->getAccessControl()->acl->pdv->inheritsRole($m, $superRoles[$m], $r)) )
                    ) {
                        if (!isset($roles[$m])) $roles[$m] = array();
                        $roles[$m][$r] = $role;
                    }
                }
            }
            //RW_Debug::dump($roles,'$roles');
        }

        $this->getServiceLocator()->get('ViewHelperManager')->get('HeadTitle')->set('Alterar usuário');

        // Configura o view
        return array(
            'userRoles' => $userRoles,
            'user'      => $user,
            'roles'     => $roles,
            'isUpdate'  => $isUpdate,
            'canUpdate' => $canUpdate,
            'modulos'   => $modulos
        );
    }

    public function aclAction()
    {
        return new ViewModel();
    }

    /**
     * Log do users
     */
    public function logAction()
    {
        // Recupera o usuário
        $idu = $this->params('idu');
        if (empty($idu)) {
            return $this->redirect()->toRoute('users');
        }

        // Verifica se o usuário existe
        $user = $this->getUserService()->fetchRow($idu);
        if (empty($user)) {
            return $this->redirect()->toRoute('users');
        }

        // Configura o view
        //$this->view->headTitle('Log usuário');
        return array(
            'user'   => $user,
            'isAjax' => $this->isAjax(),
            'log'    => $this->getUserService()->getLog()->fetchAll(array('fk_user'=>$idu))
        );
    }

    /**
     * Define se o usuário logado pode alterar o usuário
     *
     * @param array $user Informações do usuário
     *
     * @return boolean
     */
    private function _canUpdate($user)
    {
        // Verifica se é um usuário user
        if ($this->getAccessControl()->isRoot()) {
            return true;
        }

        // Verifica se tem permissão para alterar qualquer usuário
        if ($this->getAccessControl()->isBffcAllowed('user', 'user', 'update')) {
            return true;
        }

        // Verifica se é o superior alterando um subalterno ou é ele alterando ele mesmo
        // neste caso a restrição de alteração dele mesmo é feita no controller e na view
        if ($this->getAccessControl()->getUser()->id_user === $user['fk_superior'] || $this->getAccessControl()->getUser()->id_user === $user['id_user']) {
            return true;
        }

        return false;
    }

    /**
     * Define se o usuário logado pode criar usuários
     *
     * @param string $tipo Indica o tipó de usuário
     *
     * @return boolean
     */
    private function _canCreate($tipo)
    {
        // Verifica o tipo bffc
        if ($tipo === 'bffc') {
            return (  $this->getAccessControl()->isAllowed('user', 'user', 'create')
                    || $this->getAccessControl()->getUser()->role >= \Bffc\Usuario\Role::ESPECIALISTA);
        }

        // Verifica o tipo bffc
        if ($tipo === 'franqueado') {
            return ($this->getAccessControl()->isAllowed('user', 'user', 'create'));
        }

        // Verifica o tipo PDV
        if ($tipo === 'pdv') {
            return $this->_canAccess('pdv');
        }

        return false;
    }

    /**
     * Faz o upload da foto
     *
     * @param string $file
     * @param string $target
     *
     * @return boolean|string TRUE se foi feito o upload, FALSE se não tinha nada para salvar, ou a mensagem de erro
     */
    private function _uploadFoto($file, $target)
    {
        $uploadAdapter = Usuario::getUploadAdapter();

        // Verifica se foi enviada algum cupom
        if ($uploadAdapter->isUploaded($file)) {

            // Verifica se é valida
            if ( !$uploadAdapter->isValid($file) ) {
                return "Foto <b>{$uploadAdapter->getFileName($file, false)}</b> não é um arquivo jpg.";

            // Verifica se é possível fazer o upload
            } elseif ( !$uploadAdapter->receive() ) {
                return "Não foi possível salvar o arquivo: ". implode("<br>", $uploadAdapter->getMessages());
            } else {
                // Recupera o arquivo enviado
                $source = $uploadAdapter->getFileName($file);

                // Define o local final do arquivo
                $targetPath = Usuario::getFilesPath() . '/' . $target;

                // Apaga se já existir
                if (file_exists($targetPath)) {
                    $unlink = unlink($targetPath);
                }

                // Move para o local correto
                rename($source, $targetPath);

                // Coloca no tamanho correto
                $oImg = new Image($targetPath);
                $oImg->resize(Usuario::FOTO_WIDTH, Usuario::FOTO_HEIGHT, false, true);
                $oImg->save(true);

                // Retorna que foi feito o upload
                return true;
            }
        }
        return false;
    }

    /**
     * Informa se o usuário logado pode acessar o tipo de usuário
     *
     * @param string $tipo Indica o tipó de usuário
     *
     * @return boolean
     */
    private function _canAccess($tipo)
    {
        // Verifica o tipo bffc
        if ($tipo === 'bffc') {

            return ($this->getAccessControl()->isAllowed('user', 'user', 'read')
                    || $this->getAccessControl()->getUser()->role >= \Bffc\Usuario\Role::ESPECIALISTA);

        }

        // Verifica o tipo franqueado
        if ($tipo === 'franqueado') {

            return ( $this->getAccessControl()->isAllowed('user', 'user', 'read')
                    || $this->getAccessControl()->isOperador()
                    || $this->getAccessControl()->isConsultor()
                    || $this->getAccessControl()->isRegional());

        }

        // Verifica o tipo PDV
        if ($tipo === 'pdv') {

            return ( $this->getAccessControl()->isAllowed('user', 'user', 'read')
                    || $this->getAccessControl()->isAllowedPDV('cadastro', 'user')
                    || $this->getAccessControl()->isFranqueado()
                    || $this->getAccessControl()->isPdv()
                    || $this->getAccessControl()->isOperador()
                    || $this->getAccessControl()->isConsultor()
                    || $this->getAccessControl()->isRegional());
        }

        return false;
    }
}
