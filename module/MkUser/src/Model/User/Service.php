<?php
/**
 * Model básico para gerenciamento de usuários no site
 *
 * @copyright Copyright (c) 2014-2015 Realejo Design Ltda. (http://www.realejo.com.br)
 */
namespace MkUser\Model\User;

use Realejo\App\Model\Db;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;

class Service extends Db implements ServiceLocatorAwareInterface
{

    /**
     * Define a tabela a ser usada
     *
     * @var string
     */
    protected $table = 'user';

    /**
     * Define o nome da chave
     *
     * @var string
     */
    protected $key = 'id_user';

    /**
     * Não é possível apagar usuários
     */
    protected $useDeleted  = false;

    protected $showDeleted = false;

    /**
     * Verifica se é para validar a senha
     *
     * @var boolean
     */
    CONST VALIDARSENHA = true;

    /**
     * Verifica se é para validar a password forte
     *
     * @var boolean
     */
    CONST SENHAFORTE = false;

    /**
     * Verifica a quantidade de meses para trocar de password,
     * se o valor for 0 ou false ele não obriga a troca de password
     *
     * @var int
     */
    CONST TROCADESENHA = false;

    /**
     * (non-PHPdoc)
     * @see \Realejo\App\Model\Db::update()
     */
    public function update($set, $key)
    {
        // Verifica se há usuário responsável pela alteração
        $idu = null;
        if (array_key_exists('blame', $set)) {
            $idu = $set['blame'];
            unset($set['blame']);
        }

        // Salva a senha no formato certo
        if (array_key_exists('password', $set)) {
            $set['password'] = $this->createPasswordHash($set['password']);
        }

        // Verifica se há uma mensagem para o log
        $logMessage = 'Usuário alterado';
        if (array_key_exists('log', $set)) {
            $logMessage = (empty($set['log'])) ? $logMessage : $set['log'];
            unset($set['log']);
        }

        // Grava as alterações
        $update = parent::update($set, $key);
        $log    = parent::getLastUpdateDiff();

        if (count($log)>0) {
            $this->getLog()->log($key, $logMessage, 'user.update', $log, null, null, $idu);
        }

        return $update;
    }

    /**
     * Função para retorna a password encriptada de acordo
     *
     * @param string $password password a ser encriptada
     *
     * @return string
     */
    static public function createPasswordHash($password)
    {
        return md5(HASHTAG.$password.HASHTAG);
    }

    /**
     * Retorna o diretorio com os arquivos de sessão
     * @return string
     */
    public static function getSessionPath()
    {
        $config = new \Zend\Session\Config\SessionConfig();
        $sessionPath = $config->getStorageOption('save_path');

        // Verifica se a pasta da sessão
        if (!file_exists($sessionPath)) {
            $oldumask = umask(0);
            mkdir($sessionPath, 0777, true);
            umask($oldumask);
        }

        return realpath($sessionPath);
    }

    /**
     * @todo acertar o loader para fazer isso
     * @return \MkUser\Model\User\Log
     */
    public function getLog()
    {
        if (!$this->getServiceLocator()->has('\MkUser\Model\User\Log')) {
            $this->getServiceLocator()->setService('\MkUser\Model\User\Log', new \MkUser\Model\User\Log());
        }

        return $this->getServiceLocator()->get('\MkUser\Model\User\Log');
    }

    /**
     * @todo acertar o loader para fazer isso
     * @return \MkUser\Model\User\Acl
     */
    public function getAcl()
    {
        if (!$this->getServiceLocator()->has('\MkUser\Model\User\Acl')) {
            $this->getServiceLocator()->setService('\MkUser\Model\User\Acl', new \MkUser\Model\User\Acl());
        }

        return $this->getServiceLocator()->get('\MkUser\Model\User\Acl');
    }

    /**
     * Verifica se a password é forte
     *
     * @param array|String $set
     *            Dados a serem validados
     * @param array $naopode
     *            O que não pode conter na password
     * @param int $idu
     *            Código do usuário
     *
     *            return boolean | array
     */
    public function validatePassword($set, array $naopode = null, $idu = null)
    {
        $error = false;

        // Verifica se é um array
        if (is_array($set)) {
            if (isset($set['password']) && ! empty($set['password'])) {
                $password = $set['password'];
            } else {
                $error = 'Campo senha obrigatório';
            }
            // Verifica se veio algum dado
        } elseif (! empty($set)) {
            $password = $set;
        } else {
            $error = 'Campo Senha obrigatório';
        }

        // Verifica se é uma password que não pode ser usada
        if (! empty($naopode)) {
            if (array_key_exists($password, $naopode)) {
                $error = 'Esta senha não pode ser usada';
            }
        }

        // Verifica se é para buscar a password anterior
        if (! empty($idu)) {
            $user = $this->getLog()->fetchRow(array(
                'fk_user' => $idu,
                new \Zend\Db\Sql\Predicate\Expression("json like '%\"password\":" . $this->createPasswordHash($password) . "%'")
            ));
            if (! empty($user)) {
                $error = 'Esta senha ja foi usada anteriormente';
            }
        }

        // Verifica se pode fazer a validação
        if (! empty($password) && $error === false) {

            $level = 0;
            $value = preg_replace('/[ ]/', '', $password);
            $string = preg_replace('/[0-9@!#$%&*+=?|-]/', '', $value);
            $lowerCase = preg_replace('/[A-Z0-9@!#$%&*+=?|-]/', '', $value);
            $upperCase = preg_replace('/[a-z0-9@!#$%&*+=?|-]/', '', $value);
            $numbers = preg_replace('/[a-zA-Z@!#$%&*+=?|-]/', '', $value);
            $specialChars = preg_replace('/[a-zA-Z0-9]/', '', $password);

            // Validação simples
            if (! self::SENHAFORTE) {

                // Verifica se a senha tem menos que 6 caracteres
                if (strlen($password) < 6) {
                    $error = 'A senha deve conter pelo menos seis caracteres.';
                }
                // Verifica se a senha tem menos que 1 letra
                if (strlen($string) < 1) {
                    $error = 'A senha deve conter pelo menos uma letra.';
                }

                // Verifica se a senha tem menos que 1 numero
                if (strlen($numbers) < 1) {
                    $error = 'A senha deve conter pelo menos um número.';
                }

            // Verificação da password forte
            } else {
                // Da pontuação caso tenha algumas das opções abaixo
                if (strlen($lowerCase) >= 0) {
                    $level += 10;
                }
                if (strlen($upperCase) >= 0) {
                    $level += 10;
                }
                if (strlen($numbers) >= 0) {
                    $level += 10;
                }
                if (strlen($specialChars) >= 0) {
                    $level += 10;
                }

                // Da pontuação caso tenha 2 caracteres das opções abaixo
                if (strlen($lowerCase) > 2) {
                    $level += 10;
                }
                if (strlen($upperCase) > 2) {
                    $level += 10;
                }
                if (strlen($numbers) > 2) {
                    $level += 10;
                }
                if (strlen($specialChars) > 2) {
                    $level += 10;
                }

                // Da pontuação caso tenha o minimo de caracteres abaixo
                if (strlen($value) >= 6) {
                    $level += 10;
                }
                if (strlen($value) > 8) {
                    $level += 10;
                }

                if ($level <= 15) {
                    $error = 'A senha não pode ser usada pois ela é muito fraca.';
                } elseif ($level >= 16 && $level <= 30) {
                    $error = 'A senha não pode ser usada pois ela é media.';
                }
            }
        }

        // retorna ao usuário se houve erro
        return (empty($error)) ? true : $error;
    }

    /**
     * Retorna o q não pode ser uma password
     *
     * @return array
     */
    static public function getNaoSenhas()
    {
        return array(
            '111111',
            '11111111',
            '112233',
            '121212',
            '123123',
            '123456',
            '1234567',
            '12345678',
            '131313',
            '232323',
            '654321',
            '666666',
            '696969',
            '777777',
            '7777777',
            '8675309',
            '987654',
            'aaaaaa',
            'abc123',
            'abc123',
            'abcdef',
            'abgrtyu',
            'access',
            'access14',
            'action',
            'albert',
            'alexis',
            'amanda',
            'amateur',
            'andrea',
            'andrew',
            'angela',
            'angels',
            'animal',
            'anthony',
            'apollo',
            'apples',
            'arsenal',
            'arthur',
            'asdfgh',
            'asdfgh',
            'ashley',
            'asshole',
            'august',
            'austin',
            'badboy',
            'bailey',
            'banana',
            'barney',
            'baseball',
            'batman',
            'beaver',
            'beavis',
            'bigcock',
            'bigdaddy',
            'bigdick',
            'bigdog',
            'bigtits',
            'birdie',
            'bitches',
            'biteme',
            'blazer',
            'blonde',
            'blondes',
            'blowjob',
            'blowme',
            'bond007',
            'bonnie',
            'booboo',
            'booger',
            'boomer',
            'boston',
            'brandon',
            'brandy',
            'braves',
            'brazil',
            'bronco',
            'broncos',
            'bulldog',
            'buster',
            'butter',
            'butthead',
            'calvin',
            'camaro',
            'cameron',
            'canada',
            'captain',
            'carlos',
            'carter',
            'casper',
            'charles',
            'charlie',
            'cheese',
            'chelsea',
            'chester',
            'chicago',
            'chicken',
            'cocacola',
            'coffee',
            'college',
            'compaq',
            'computer',
            'cookie',
            'cooper',
            'corvette',
            'cowboy',
            'cowboys',
            'crystal',
            'cumming',
            'cumshot',
            'dakota',
            'dallas',
            'daniel',
            'danielle',
            'debbie',
            'dennis',
            'diablo',
            'diamond',
            'doctor',
            'doggie',
            'dolphin',
            'dolphins',
            'donald',
            'dragon',
            'dreams',
            'driver',
            'eagle1',
            'eagles',
            'edward',
            'einstein',
            'erotic',
            'extreme',
            'falcon',
            'fender',
            'ferrari',
            'firebird',
            'fishing',
            'florida',
            'flower',
            'flyers',
            'football',
            'forever',
            'freddy',
            'freedom',
            'fucked',
            'fucker',
            'fucking',
            'fuckme',
            'fuckyou',
            'gandalf',
            'gateway',
            'gators',
            'gemini',
            'george',
            'giants',
            'ginger',
            'golden',
            'golfer',
            'gordon',
            'gregory',
            'guitar',
            'gunner',
            'hammer',
            'hannah',
            'hardcore',
            'harley',
            'heather',
            'helpme',
            'hentai',
            'hockey',
            'hooters',
            'horney',
            'hotdog',
            'hunter',
            'hunting',
            'iceman',
            'iloveyou',
            'internet',
            'iwantu',
            'jackie',
            'jackson',
            'jaguar',
            'jasmine',
            'jasper',
            'jennifer',
            'jeremy',
            'jessica',
            'johnny',
            'johnson',
            'jordan',
            'joseph',
            'joshua',
            'junior',
            'justin',
            'killer',
            'knight',
            'ladies',
            'lakers',
            'lauren',
            'leather',
            'legend',
            'letmein',
            'letmein',
            'little',
            'london',
            'lovers',
            'maddog',
            'madison',
            'maggie',
            'magnum',
            'marine',
            'marlboro',
            'martin',
            'marvin',
            'master',
            'matrix',
            'matthew',
            'maverick',
            'maxwell',
            'melissa',
            'member',
            'mercedes',
            'merlin',
            'michael',
            'michelle',
            'mickey',
            'midnight',
            'miller',
            'mistress',
            'monica',
            'monkey',
            'monkey',
            'monster',
            'morgan',
            'mother',
            'mountain',
            'muffin',
            'murphy',
            'mustang',
            'naked',
            'nascar',
            'nathan',
            'naughty',
            'ncc1701',
            'newyork',
            'nicholas',
            'nicole',
            'nipple',
            'nipples',
            'oliver',
            'orange',
            'packers',
            'panther',
            'panties',
            'parker',
            'password',
            'password',
            'password1',
            'password12',
            'password123',
            'patrick',
            'peaches',
            'peanut',
            'pepper',
            'phantom',
            'phoenix',
            'player',
            'please',
            'pookie',
            'porsche',
            'prince',
            'princess',
            'private',
            'purple',
            'pussies',
            'qazwsx',
            'qwerty',
            'qwertyui',
            'rabbit',
            'rachel',
            'racing',
            'raiders',
            'rainbow',
            'ranger',
            'rangers',
            'rebecca',
            'redskins',
            'redsox',
            'redwings',
            'richard',
            'robert',
            'rocket',
            'rosebud',
            'runner',
            'rush2112',
            'russia',
            'samantha',
            'sammy',
            'samson',
            'sandra',
            'saturn',
            'scooby',
            'scooter',
            'scorpio',
            'scorpion',
            'secret',
            'sexsex',
            'shadow',
            'shannon',
            'shaved',
            'sierra',
            'silver',
            'skippy',
            'slayer',
            'smokey',
            'snoopy',
            'soccer',
            'sophie',
            'spanky',
            'sparky',
            'spider',
            'squirt',
            'srinivas',
            'startrek',
            'starwars',
            'steelers',
            'steven',
            'sticky',
            'stupid',
            'success',
            'suckit',
            'summer',
            'sunshine',
            'superman',
            'surfer',
            'swimming',
            'sydney',
            'taylor',
            'tennis',
            'teresa',
            'tester',
            'testing',
            'theman',
            'thomas',
            'thunder',
            'thx1138',
            'tiffany',
            'tigers',
            'tigger',
            'tomcat',
            'toutcard',
            'topgun',
            'toyota',
            'travis',
            'trouble',
            'trustno1',
            'tucker',
            'turtle',
            'twitter',
            'united',
            'vagina',
            'victor',
            'victoria',
            'viking',
            'voodoo',
            'voyager',
            'walter',
            'warrior',
            'welcome',
            'whatever',
            'william',
            'willie',
            'wilson',
            'winner',
            'winston',
            'winter',
            'wizard',
            'xavier',
            'xxxxxx',
            'xxxxxxxx',
            'yamaha',
            'yankee',
            'yankees',
            'yellow',
            'zxcvbn',
            'zxcvbnm',
            'zzzzzz',
            'botafogo',
            'flamengo',
            'vasco',
            'corintians',
            'curintcha',
            '11111a',
            'aaaaa1'
        );
    }

    /**
     * Service Locator, tem que substituir o Loader por ele
     * @var ServiceLocatorInterface
     */
    protected $services;

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->services = $serviceLocator;
    }

    public function getServiceLocator()
    {
        return $this->services;
    }
}