<?php

namespace MkUser\Mapper;

use ZfcUser\Mapper\User as ZfcUser;
use Zend\Stdlib\Hydrator\HydratorInterface;

class User extends ZfcUser
{
    protected $tableName  = 'user';

    public function findById($id)
    {
        $select = $this->getSelect()
                       ->where(array('id_user' => $id));

        $entity = $this->select($select)->current();
        $this->getEventManager()->trigger('find', $this, array('entity' => $entity));
        return $entity;
    }

    public function update($entity, $where = null, $tableName = null, HydratorInterface $hydrator = null)
    {
        if (!$where) {
            $where = array('id_user' => $entity->getId());
        }

        return parent::update($entity, $where, $tableName, $hydrator);
    }
}
