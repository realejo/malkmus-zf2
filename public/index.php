<?php
/**
 * This makes our life easier when dealing with paths. Everything is relative
 * to the application root now.
 */
chdir(dirname(__DIR__));

// Decline static file requests back to the PHP built-in webserver
if (php_sapi_name() === 'cli-server') {
    $path = realpath(__DIR__ . parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
    if (__FILE__ !== $path && is_file($path)) {
        return false;
    }
    unset($path);
}

// Define application environment
if (PHP_SAPI  === 'cli') {
    // Linux Server (PRODUCTION)
    if ( isset($_SERVER['PWD']) && strpos($_SERVER['PWD'], 'xxxxx') !== false  || strpos($_SERVER['SCRIPT_NAME'], 'xxx') !== false) {
        define('APPLICATION_ENV','production');

        // Linux Server (BETA)
    } elseif ( isset($_SERVER['PWD']) && strpos($_SERVER['PWD'], 'xxxbeta') !== false  || strpos($_SERVER['SCRIPT_NAME'], 'xxxbeta') !== false) {
        //define('APPLICATION_ENV', 'beta');
        define('APPLICATION_ENV', 'production');

        // Petunia Server
    } elseif ( strpos($_SERVER['PWD'], 'sites/realejo/cms-zf2') !== false || strpos($_SERVER['SCRIPT_NAME'], 'sites/realejo/cms-zf2') !== false) {
        define('APPLICATION_ENV', 'development');

        // Others
    } else {
        define('APPLICATION_ENV', 'local');
    }

} else {
    // Linux Server
    if ( $_SERVER['SERVER_ADDR'] === 'xxx.xxx.xxx.xxx' ) {

        define('APPLICATION_ENV','production');

        /* if ( strpos($_SERVER['SERVER_NAME'], 'beta') === false) {
         define('APPLICATION_ENV','production');
         } else {
         define('APPLICATION_ENV','beta');
         } */

    // Servidores locais
    } elseif ( strpos($_SERVER['SERVER_ADDR'], '192.168.100') !== false) {
        define('APPLICATION_ENV','development');

        // Others
    } else {
        define('APPLICATION_ENV', 'local');
    }
}

// HASH para usar nas encriptação de senhas
define("HASHTAG", sha1('maldita p@radiso K(Y^X)'));

// Required for Realejo Libs
define('APPLICATION_DATA', realpath(__DIR__ . '/../data'));

// Required for application paths
define('APPLICATION_ROOT', realpath(__DIR__).'/..');

// Define path to public
defined('APPLICATION_HTTP')
|| define('APPLICATION_HTTP', realpath(__DIR__));

// Setup autoloading
require 'init_autoloader.php';

// Run the application!
Zend\Mvc\Application::init(require 'config/application.config.php')->run();
