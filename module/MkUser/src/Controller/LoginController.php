<?php
/**
 * Universidade BFFC (http://universidade.bffc.com.br)
 *
 * @copyright Copyright (c) 2013 Realejo (http://realejo.com.br)
 */
namespace MkUser\Controller;

use MkUser\Model\Controller\ActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container as SessionContainer;
use Zend\Authentication\AuthenticationService;

class LoginController extends ActionController
{

    /**
     * Número máximo de tentativas antes de pedir o captcha
     *
     * @var int
     */
    private $_maxTentativas = 200;

    public function indexAction()
    {
        // Verifica se já está logado
        if ($this->getAccessControl()->hasIdentity()) {

            // Redireciona para a página inicial
            return $this->redirect()->toUrl('/');
        }

        // Verifica o número de tentativas
        $controle = new SessionContainer('controle');
        if (! isset($controle->tentativas)) {
            $controle->tentativas = 1;
        }

        // Carrega o recaptcha
        // $recaptcha = new Zend_Service_ReCaptcha(LoginController::PUBKEY, LoginController::PRIVKEY);

        $email = $this->getRequest()->getPost('email', $this->getRequest()->getQuery('email'));

        if ($this->getRequest()->isPost()) {
            $erro = false;

            // Verifica quantas tentivas
            if ($controle->tentativas > $this->_maxTentativas) {

                // retornar o que foi digitado no captcha
                $response = $this->getRequest()->getPost('recaptcha_response_field');

                // verifica se foi digitado o código
                if (empty($response)) {
                    $this->userMessenger()->addErrorMessage('Você precisa informa o código.');
                    $erro = true;
                } else {

                    // verifica o que foi digitado no campo
                    $recaptcha = new \Zend\Captcha\ReCaptcha();
                    $result = $recaptcha->verify($this->getRequest()
                                        ->getPost('recaptcha_challenge_field'), $response);

                    // valida o que foi digitado no captcha
                    if (! $result->isValid()) {
                        $this->userMessenger()->addErrorMessage('error', 'Código inválido.');
                        $erro = true;
                    }
                }
            } // Fim do controle de tentativas

            $password = $this->getRequest()->getPost('password');

            // Verifica se preencheu os campos
            if (! $erro) {
                if (empty($email) || empty($password)) {
                    $this->userMessenger()->addErrorMessage('Você deve informar seu email e password.');
                    $erro = true;
                    $controle->tentativas ++;
                }
            }

            // Verifica o login
            if (! $erro) {

                // Configura o Zend_Authenticate
                $auth = new AuthenticationService();
                $adapter = new \MkUser\Model\Authentication\Adapter($email, $password);
                $adapter->setServiceLocator($this->getServiceLocator());
                $auth->setAdapter($adapter);
                $result = $auth->authenticate();

                // Verifica se o login é valido
                if ($result->isValid()) {

                    // recupera o usuário
                    $user = $auth->getAdapter()->getUser();

                    // Grava os dados da sessão
                    $auth->getStorage()->write($user);

                    // Zera as tentativas
                    $controle->tentativas = 0;

                    // Redireciona para a página inicial
                    return $this->redirect()->toUrl('/');
                } // end if ($result->isValid()) {

                // Retorna o erro
                $controle->tentativas ++;
                $erros = $result->getMessages();

                // Grava as mensagem de erro para o usuário
                foreach ($erros as $e) {
                    $this->userMessenger()->addErrorMessage($e);
                    // Só mostra os seguintes se for da realejo
                    // pois são mensagens para identificar o erro
                    if (!$this->isRealejo()) {
                        break;
                    }
                } // end foreach ($erros as $e) {
            }
        }

        // Configura o view
        // $this->view->email = $email;
        // $this->view->recaptcha = $recaptcha;
        // $this->view->showCaptcha = ($controle->tentativas > $this->_maxTentativas);

        // Define o título
        // $this->view->headTitle("Login | ". APPLICATION_NAME);

        // Inicializa o Js
        $this->getVendor()->jsValidate();

        // Verifica se está logado
        return new ViewModel(array(
            'email' => $email
        ));
    }

    public function esqueciAction()
    {
        // Verifica se já está logado
        if ($this->getAccessControl()->hasIdentity()) {

            // Redireciona para a página inicial
            return $this->redirect()->toUrl('/');
        }

        $email = $this->getRequest()->getPost('email', $this->getRequest()
                                                            ->getQuery('email'));

        $erro = $enviado = false;
        if ($this->getRequest()->isPost()) {

            if (empty($email) || ! \Realejo\Mail::isEmail($email)) {
                $this->userMessenger()->addErrorMessage('Email inválido.');
                $erro = true;
            }

            // Verifica se é um funcionário bobs
            if (strpos($email, '@bffc.com.br') || strpos($email, '@bobs.com.br')) {
                $this->userMessenger()->addInfoMessage("Funcionários BFFC devem usar o seu login da Extranet.Caso não lembre, <a href='http://extranet.bffc.com.br/login/esqueci?email=$email'>clique aqui</a>");
                $erro = true;
            }
            if ( !$erro ) {
                $lookup = $this->getUserService()->fetchRow(array('email' => $email));

                if ( empty($lookup) ) {
                    $this->userMessenger()->addErrorMessage("Email não encontrado");
                    $erro = true;
                } elseif (!empty($lookup['fk_extranet'])) {
                    $this->userMessenger()->addInfoMessage('Você deve usar o seu login da Extranet BFFC. Caso não lembre, <a href="http://extranet.bffc.com.br/login/esqueci?email='. $email . '">clique aqui</a>');
                } else {
                    // Gera o código único
                    $codigo = md5($lookup['id_user'].$email.HASHTAG.time());

                    // Inclui o código no cadastro do usuário
                    $this->getUserService()->update(array('codigo'=>$codigo), $lookup['id_user']);

                    $message = "<p>Para iniciar o processo de redefinição de password da sua Conta do Universidade Corporativa $email, clique no link abaixo:</p>
                                <p>http://skeleton.realejo.local/login/redefinir/$codigo</p>
                                <p>Se o link não funcionar, copie e cole o URL em uma nova janela do navegador.</p>
                                <p>Caso tenha recebido esse e-mail por engano, provavelmente outro usuário inseriu seu endereço de e-mail ao tentar redefinir uma password. Caso não tenha feito a solicitação, você não precisa tomar nenhuma ação e pode desconsiderar este e-mail com segurança.</p>
                                <p>Se você tiver alguma dúvida sobre sua conta, entre em contato com o seu gerente.</p>
                                <p></p>
                                <p>Este e-mail foi enviado automaticamente. Não responda a esta mensagem.</p>";

                    $headers  = 'MIME-Version: 1.0' . "\r\n";
                    $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";

                    // Additional headers
                    $headers .= 'To: '.$lookup['nome'].' <'.$lookup['email'].'> ' . "\r\n";
                    $headers .= 'From: Não Responder <naoresponder@skeleton.realejo.local>' . "\r\n";

                    // Mail it
                    mail($lookup['email'], 'Recuperação de password', $message, $headers);


                    /* Cria o email
                    $oMailer = new RW_Mail();
                    $message = $this->view->partial(
                            'login/_esqueci.phtml',
                            array(
                                    'codigo' => $codigo,
                                    'email'  => $email
                            )
                    );

                    // Envia o email
                    $oMailer->SendEmail(
                            null, // usar o nome padrão
                            null, // usar o email padrão
                            $lookup['nome'],
                            $lookup['email'],
                            "Recuperação de password",
                            array('html'=>$message));
                    */

                    // Marca como enviado
                    $enviado = "Foi enviada uma mensagem para o email <b>$email</b> com instruções para alterar a sua password.";
                }
            }
        }

        // Verifica se está logado
        return new ViewModel(array(
                //'id' => $captcha->generate(),
                //'captcha' => $captcha,
                'enviado' => $enviado,
                'email'   => $email
        ));

        // Inicializa o Js
        $this->getVendor()->jsValidate();

        $this->getServiceLocator()->get('ViewHelperManager')->get('HeadTitle')->set("Recuperar Senha");
    }

    public function redefinirAction()
    {
        // Marca como não enviado
        $erro       = $enviado = false;
        $codigo     = $this->params('codigo');
        $naopasswords  = \MkUser\Model\User::getNaoSenhas();
        if (!empty($codigo)) {
            $user  = $this->getUserService()->fetchRow(array("codigo"=>$codigo));
        } elseif ($this->getAccessControl()->hasIdentity()) {
            $user = $this->getAccessControl()->getUser(true);
        } elseif (!$this->getAccessControl()->hasIdentity() && empty($codigo)) {
            // Redireciona para a página inicial
            return $this->redirect()->toUrl('/');
        }

        if ($this->getRequest()->isPost()) {

            // Verifica se a password á valida
            $password  = $this->getRequest()->getPost('password');
            $password1 = $this->getRequest()->getPost('password1');
            if ( $password == '' || empty($password)) {
                $this->userMessenger()->addErrorMessage("Senha inválida");
                $erro = true;
            } elseif ( $password != $password1 ) {
                $this->userMessenger()->addErrorMessage("A password e a confirmação não são iguais");
                $erro = true;
            }

            if (\MkUser\Model\User::VALIDARSENHA && !$erro) {
                $isValid = $this->getUserService()->validateSenha($password, $naopasswords, $user['id_user']);
                if ($isValid !== true) {
                    $this->userMessenger()->addErrorMessage($isValid);
                    $erro = true;
                }
            }


            if ( !$erro ) {
                $this->getUserService()->update(
                    array(
                        'password'         => $this->getUserService()->createPasswordHash($password),
                        'password_recriar' => 0,
                        'codigo'        => null
                    ),
                    $user['id_user']
                );

                // Grava o log
                $this->getUserService()->getLog()->log($user['id_user'], 'Troca de password efetuada', 'redefinir.password',
                    array('REMOTE_ADDR'     =>  $_SERVER['REMOTE_ADDR'],
                        'HTTP_USER_AGENT' =>  $_SERVER['HTTP_USER_AGENT']
                ));
                $enviado = 'A nova password foi alterada.';
            }
        }

        $this->getServiceLocator()->get('ViewHelperManager')->get('HeadTitle')->set("Redefinindo Senha");

        // Inicializa o Js
        $this->getVendor()->jsValidate();

        // Verifica se está logado
        return new ViewModel(array(
                //'id' => $captcha->generate(),
                'naopasswords' => $naopasswords,
                'user'   => $user,
                'enviado'   => $enviado
        ));
    }

    public function logoutAction()
    {
        $this->getAccessControl()->clearIdentity();

        $this->redirect()->toUrl('/');

        return false;
    }
}

