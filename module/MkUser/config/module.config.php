<?php
return array(
    'router' => array(
        'routes' => array(
            'users' => array(
                'type'    => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    // Change this to something specific to your module
                    'route'    => '/users',
                    'defaults' => array(
                        // Change this value to reflect the namespace in which
                        // the controllers for your module are found
                        'controller'    => 'mkusers',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'user' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/user/:idu',
                            'defaults' => array(
                                'controller' => 'mkuser',
                                'action'     => 'index',
                            ),
                            'constraints' => array('idu' => '[0-9]*'),
                        ),
                    ), // end /users/user/:idu
                    'user-personal' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/user-personal/:idu',
                            'defaults' => array(
                                'controller' => 'mkuser',
                                'action' => 'personal',
                            ),
                            'constraints' => array('idu' => '[0-9]*'),
                        ),
                    ), // end /users/user-personal
                    'user-permission' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/user-permission/:idu',
                            'defaults' => array(
                                'controller' => 'mkuser',
                                'action' => 'permission',
                            ),
                            'constraints' => array('idu' => '[0-9]*'),
                        ),
                    ), // end /users/user-permission
                    'user-log' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/user-log/:idu',
                            'defaults' => array(
                                'controller' => 'mkuser',
                                'action' => 'log',
                            ),
                            'constraints' => array('idu' => '[0-9]*'),
                        ),
                    ), // end /users/end user-log
                ), // end /users/child_routes
            ), // end /users

            'login' => array(
                'type'    => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/login',
                    'defaults' => array(
                        'controller'    => 'mklogin',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'logout' => array(
                        'type' => 'Zend\Mvc\Router\Http\Literal',
                        'options' => array(
                            'route' => '/logout',
                            'defaults' => array(
                                'controller' => 'mklogin',
                                'action' => 'logout'
                            )
                        )
                    ), // end /login/logout
                    'esqueci' => array(
                        'type' => 'Zend\Mvc\Router\Http\Literal',
                        'options' => array(
                            'route' => '/esqueci',
                            'defaults' => array(
                                'controller' => 'mklogin',
                                'action'     => 'esqueci'
                            )
                        )
                    ), // end /login/esqueci
                    'redefinir' => array(
                        'type' => 'Zend\Mvc\Router\Http\Segment',
                        'options' => array(
                            'route' => '/redefinir[/:codigo]',
                            'defaults' => array(
                                'controller' => 'mklogin',
                                'action'     => 'redefinir'
                            )
                        )
                    ), // end /login/redefinir
                ) // end /login/child_routes
            ), // end /login
        ),
    ), // end router
    'controller_plugins' => array(
        'invokables' => array(
            'UserMessenger' => 'MkUser\Model\Controller\Plugin\UserMessenger'
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'MkUser\Controller\Login' => 'MkUser\Controller\LoginController',
            'MkUser\Controller\Users' => 'MkUser\Controller\UsersController',
            'MkUser\Controller\User'  => 'MkUser\Controller\UserController',
        ),
        'factories' => array(
            'mkuser'  => 'MkUser\Factory\Controller\UserControllerFactory',
            'mkusers' => 'MkUser\Factory\Controller\UsersControllerFactory',
            'mklogin' => 'MkUser\Factory\Controller\LoginControllerFactory',
        ),
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory'
        ),
        'invokables' => array(
            'mkuser_user_service'           => 'MkUser\Service\User',
            'mkuser_module_options'         => 'MkUser\Options\ModuleOptions',
            'mkuser_accesscontrol'          => 'MkUser\Model\AccessControl',
            'mkuser_log'                    => 'MkUser\Model\MkUser\Log',
            'mkuser_acl'                    => 'MkUser\Model\MkUser\Acl',
            'mkuser_state'                  => 'MkUser\Model\MkUser\Status',
            'mkuser_authentication_adapter' => 'MkUser\Model\Authentication\Adapter',
        ),
        'factories' => array(
            'mkuser_user_hydrator' => 'MkUser\Factory\UserHydrator',
            'mkuser_user_mapper'   => 'MkUser\Factory\UserMapperFactory',
            'mkuser_user_form'     => 'MkUser\Factory\Form\UserFormFactory',

            'Zend\Authentication\AuthenticationService' => function($sm) {
            // Create your authentication service!
            },
            'MkUser\Vendor' => function($sm) {
            return new \MkUser\Model\Vendor($sm);
            }
        ),
        'aliases' => array(
        ),
    ),
    /**
     * O site não usa o translator, mas precisa ter por caus de umbug no view helper
     * @see https://github.com/zendframework/zf2/issues/4626
     */
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ),
        ),
    ),
    'view_helpers' => array(
        'invokables' => array(
            'getAccessControl'      => 'MkUser\Model\View\Helper\GetAccessControl',
            'checkCanUpdate'        => 'MkUser\Model\View\Helper\CheckCanUpdate',
            'showMessages'          => 'MkUser\Model\View\Helper\ShowMessages',
            'showFormErrorControl'  => 'MkUser\Model\View\Helper\ShowFormErrorControl',
            'CKEditor'              => 'MkUser\Model\View\Helper\CKEditor',
        )
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'template_map' => include __DIR__  .'/../template_map.php',
        'template_path_stack' => array(
            __DIR__ . '/../view'
        ),
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
);
