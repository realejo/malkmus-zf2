<?php
/**
 * Universidade BFFC (http://universidade.bffc.com.br)
 *
 * @copyright Copyright (c) 2013 Realejo (http://realejo.com.br)
 */
namespace MkUser\Model\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\Json\Encoder;

/**
 * View helper plugin to fetch the authenticated identity.
 */
class CKEditor extends AbstractHelper
{

    CONST CKEDITOR = "ckeditor-4.5.1";
    CONST CKFINDER = "_ckfinder-2.3.1";

    /**
     * Retorna o CKEditor
     */
    public function __invoke($campos = array(), $options = array())
    {
        $path = "/vendor/";
        $ckeditor = $path . self::CKEDITOR . '/';
        $ckfinder = $path . self::CKFINDER ;

        // Verifica se deve usar o CKFinder
        if ( array_key_exists ('ckfinder', $options ) ) {
            $options['filebrowserBrowseUrl']       = $ckfinder . '/ckfinder.html';
            $options['filebrowserImageBrowseUrl']  = $ckfinder . '/ckfinder.html?Type=Images';
            $options['filebrowserUploadUrl']       = $ckfinder . '/core/connector/php/connector.php?command=QuickUpload&type=Files';
            $options['filebrowserImageUploadUrl']  = $ckfinder . '/core/connector/php/connector.php?command=QuickUpload&type=Images';

            unset($options['ckfinder']);
        }

        // Verifica os inputs que deve colocar o CKEditor
        if ( !is_array($campos) && is_string($campos) ) $campos = array($campos);

        // Formata as configurações
        $cloneOptions = $options;

        if ( array_key_exists('validator', $cloneOptions) ) {
            unset($options['validator']);
        }

        $options = (empty($options)) ? '{}' : Encoder::encode($options);

        // Carrega as opções para cada campo
        $config = '';

        foreach($campos as $c) {
            if( ! array_key_exists('validator', $cloneOptions) ) {
                $config .= "$( '$c' ).ckeditor(function() {}, $options);";
            } else {
                $config .= "$( '$c' ).ckeditor(function() {}, $options).editor.on('change', function() { $('{$cloneOptions['validator']['form']}').bootstrapValidator('revalidateField', '{$cloneOptions['validator']['name']}'); });";
            }
        }

        // Cria a configuração do CKEditor
        $script = "$(document).ready(function(){ $config });";

        // Carrega a biblioteca do CKEditor
        $this->getView()->headScript()->appendFile($ckeditor . 'ckeditor.js', 'text/javascript', array('minify_disabled' => true));

        // Carrega o JQuery Adapter
        $this->getView()->headScript()->appendFile($ckeditor . 'adapters/jquery.js', 'text/javascript', array('minify_disabled' => true));

        // Carrega o código CKEditor
        //$this->getView()->headScript()->appendScript($script, 'text/javascript', array('minify_disabled' => true));
        return $script;
    }
}
