<?php
/**
 * Plugin para mostrar as mensagens ao usuário na view.
 * Ele extende o plugin FlashMessenger alterando o controle de sessão
 *  pois ele só mostrar as mensagens no próximo request
 *
 * @copyright Copyright (c) 2013 Realejo (http://realejo.com.br)
 */
namespace MkUser\Model\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\FlashMessenger;

class UserMessenger extends FlashMessenger
{
}
