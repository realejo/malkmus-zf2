<?php
/**
 * Model para gerenciar o log dos usuários
 *
 * @copyright Copyright (c) 2014-2015 Realejo Design Ltda. (http://www.realejo.com.br)
 */
namespace MkUser\Model\User;

use Realejo\App\Model\Db as AppModel;

class Log extends AppModel
{

    protected $table = 'user_log';

    protected $key   = 'id_log';

    protected $order = 'id_log desc';

    /**
     * Não é possível apagar usuários
     */
    protected $useDeleted  = false;

    /**
     * Grava o log para um usuário
     *
     * @param int    $idu       Código do usuário
     * @param string $description Descrição do log
     * @param string $type      OPCIONAL Tipo para busca de log
     * @param array  $info      OPCIONAL Dados adicionais para guardar no log
     * @param int    $idRef     OPCIONAL Usuário de referencia, que executou a ação
     *
     * @return int código do log criado
     */
    public function log($idu, $description, $type = null, $info = null, $idRef = null)
    {
        // Verifica se o $dados foi usado para passar o $idRef
        if (is_null($idRef) && is_numeric($info)) {
            $idRef = $info;
            $info = null;
        }

        // Define o log
        $log = array(
            'fk_user'     => $idu,
            'date'        => date('Y-m-d H:i:s'),
            'description' => $description,
            'type'        => $type,
            'fk_user_ref' => $idRef,
            'json'        => (is_null($info)) ? null : \Zend\Json\Json::encode($info)
        );

        // Grava o log
        $return = $this->insert($log);

        // Limpa o cache
        if ($this->getUseCache()) {
            $this->getCache()->clean();
        }

        // Retorna a chave do log criado
        return $return;
    }

    /**
     * Retorna o Select com o usuário que fez a alteração no join
     *
     * @return \Zend\Db\Sql\Select
     */
    public function getSQLSelect()
    {
        return $this->getTableGateway()
                    ->getSql()
                    ->select()
                    ->join('user',
                           'user_log.fk_user_ref=user.id_user',
                           array('reference' => 'name'),
                           \Zend\Db\Sql\Select::JOIN_LEFT
                    );
    }

    /**
     * Decodifica o json quando não estiver usando a paginação
     *
     * @param array $fetchAll
     *
     * @return array
     */
    protected function getFetchAllExtraFields($fetchAll, $where=null)
    {
        if (!empty($fetchAll)) {
            foreach ($fetchAll as $id => $row) {
                if (! empty($row['json'])) {
                    $fetchAll[$id]['json'] = \Zend\Json\Json::decode($row['json'], \Zend\Json\Json::TYPE_ARRAY);
                }
            }
        }

        return $fetchAll;
    }
}
