<?php
namespace MkUser\Controller;

use MkUser\Model\Controller\ActionController;

class UsersController extends ActionController
{
    /**
     * Verifica se tem acesso
     */
    public function onDispatch(\Zend\Mvc\MvcEvent $e)
    {
        // Verifica se tem permissão de estar aqui
        if (!$this->getAccessControl()->hasIdentity()) {
            return $this->redirect()->toRoute('login');
        }

        return parent::onDispatch($e);
    }

    /**
     * Action para alterar o user
     */
    public function indexAction()
    {
        // Verifica se o usuário existe
        $users = $this->getUserService()->fetchAll();

        $this->getServiceLocator()->get('ViewHelperManager')->get('HeadTitle')->set('Users');

        // Configura o view
        return array(
            'users' => $users
        );

    }

}
