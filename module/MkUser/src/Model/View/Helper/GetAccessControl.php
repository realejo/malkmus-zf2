<?php
/**
 * Universidade BFFC (http://universidade.bffc.com.br)
 *
 * @copyright Copyright (c) 2013 Realejo (http://realejo.com.br)
 */
namespace MkUser\Model\View\Helper;

use Zend\View\Helper\AbstractHelper;

/**
 * View helper plugin to fetch the authenticated identity.
 */
class GetAccessControl extends AbstractHelper
{

    /**
     * Controle de acesso
     *
     * @var \Application\Model\AccessControl
     */
    protected $accessContol;

    /**
     * Retorna o controle de acesso
     *
     * @return \MkUser\Model\AccessControl
     */
    public function __invoke()
    {
        if (! $this->accessContol instanceof \MkUser\Model\AccessControl) {
            $this->accessContol = new \MkUser\Model\AccessControl();
        }

        return $this->accessContol;
    }
}
