<?php
/**
 * Universidade BFFC (http://universidade.bffc.com.br)
 *
 * @copyright Copyright (c) 2013 Realejo (http://realejo.com.br)
 */
namespace MkUser\Model\View\Helper;

use MkUser\Model\Controller\Plugin\UserMessenger;
use Zend\View\Helper\FlashMessenger;

/**
 * View helper plugin to fetch the authenticated identity.
 */
class ShowMessages extends FlashMessenger
{
    /**
     * Render Messages
     *
     * @param  string $namespace
     * @param  array  $classes
     * @return string
     */
    public function __invoke($namespace = null)
    {
        // Configura o BS3
        $this->setMessageOpenFormat('<div%s>')
             ->setMessageSeparatorString('</div><div%s>')
             ->setMessageCloseString('</div>');

        $render = '';
        if ($namespace == null || $namespace == 'error') {
            $render .= parent::render('error',   array('alert', 'alert-danger'));
        }
        if ($namespace == null || $namespace == 'info') {
            $render .= parent::render('info',    array('alert', 'alert-info'));
        }
        if ($namespace == null || $namespace == 'default') {
            $render .= parent::render('default', array('alert', 'alert-warning'));
        }
        if ($namespace == null || $namespace == 'success') {
            $render .= parent::render('success', array('alert', 'alert-success'));
        }

        return $render;
    }

    /**
     * Retrieve the escapeHtml helper
     *
     * @return EscapeHtml
     */
    protected function getEscapeHtmlHelper()
    {
        $this->escapeHtmlHelper = function($html) {
            return $html;
        };

        return $this->escapeHtmlHelper;
    }
}
