<?php
/**
 * Model para gerenciar os videos
 *
 * @copyright Copyright (c) 2013 Realejo (http://realejo.com.br)
 */
namespace MkUser\Model\User;

use Application\Model\StatusAbstract;
use Application;

class Status extends StatusAbstract
{
    CONST APROVADO  = 'A';
    CONST BLOQUEADO = 'B';

    static protected $status = array(
        self::APROVADO   => array('Aprovado',  'Aprovado'),
        self::BLOQUEADO  => array('Bloqueado', 'Bloqueado'),

    );

}
