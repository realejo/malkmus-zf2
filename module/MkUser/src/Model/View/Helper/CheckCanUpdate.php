<?php
/**
 * Verifica se tem acesso a determindao recurso
 */
namespace MkUser\Model\View\Helper;

use Zend\View\Helper\AbstractHelper;

class CheckCanUpdate extends AbstractHelper
{
    /**
     * Retorna se tem acesso
     *
     * @param string $key      chave a ser verificada
     * @param array $canUpdate lista de permissões
     *
     * @return boolean
     */
    public function __invoke($key, $canUpdate)
    {
        if ($canUpdate === true) {
            return true;
        }

        if (empty($canUpdate) || !is_array($canUpdate)) {
            return false;
        }

        if (in_array($key, $canUpdate, true))  {
            return true;
        }

        if (!is_numeric($key) && array_key_exists($key, $canUpdate)) {
            return ($canUpdate[$key] !== false);
        }

        return false;
    }
}
