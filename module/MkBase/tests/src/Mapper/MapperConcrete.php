<?php
namespace MkBaseTest\Mapper;

use MkBase\Mapper\MapperAbstract;

class MapperConcrete extends MapperAbstract
{
    protected $tableName = 'album';
    protected $tableKey  = 'id';
}
