<?php
/**
 * Universidade BFFC (http://universidade.bffc.com.br)
 *
 * @copyright Copyright (c) 2013 Realejo (http://realejo.com.br)
 */
namespace MkUser\Model\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Stdlib\RequestInterface as Request;
use Zend\Stdlib\ResponseInterface as Response;

abstract class ActionController extends AbstractActionController
{
    /**
     * @var \MkUser\Model\AccessControl
     */
    private $_accessControl;

    public function dispatch(Request $request, Response $response = null)
    {
        parent::dispatch($request, $response);

        // Configura as chamadas Ajax
        if ($this->isAjax()) {
            $this->layout('layout/layout-ajax');
        }
    }

    public function isAjax()
    {
        return ( $this->getRequest()->isXmlHttpRequest() || ($this->getRequest()->getQuery('ajax',0) == 1) );
    }

    public function isRealejo()
    {
        return ( APPLICATION_ENV !== 'production' || $_SERVER['REMOTE_ADDR'] === '200.142.107.74');
    }

    /**
     * @return \MkUser\Model\AccessControl
     */
    public function getAccessControl()
    {
        if (!isset($this->_accessControl)) {
            $this->_accessControl = new \MkUser\Model\AccessControl();
        }

        return $this->_accessControl;
    }

    /**
     * retorna o model no formato jsonp
     * @param mixed $valueToSend valores a serem codificados em JSON e enviados
     * @todo varificar XXS com getQuery('callback') e _getParam('_')
     */
    public function sendJsonP($valueToSend)
    {
        $json = new \Zend\View\Model\JsonModel($valueToSend);
        $json->setJsonpCallback($this->getRequest()->getQuery('callback'));
        return $json;
    }

    /**
     * @return \MkUser\Service\User
     */
    public function getUserService()
    {
        return $this->getServiceLocator()->get('mkuser_user_service');
    }

    /**
     * @return \Application\Model\Vendor
     */
    public function getVendor()
    {
        return $this->getServiceLocator()->get('MkUser\Vendor');
    }
}
