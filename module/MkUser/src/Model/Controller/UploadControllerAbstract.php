<?php
namespace MkUser\Model\Controller;

abstract class UploadControllerAbstract extends ActionController
{

    /**
     * Instancia do model.
     *
     * @var unknown
     */
    private $oModel;

    /**
     * Instancia do loader.
     *
     * @var unknown
     */
    private $oLoader;

    /**
     * Id do model pai.
     *
     * @var integer
     */
    private $id;

    public function setUploadValidator($extensoes = array())
    {

        // Se for passado extensões para validar
        if (!empty($extensoes)) {

            foreach ($extensoes as $ext) {
                $this->getUploadAdapter()->addValidator('extension', false, $ext);
            }

        }
    }

    public function getUploadModel()
    {
        return $this->oModel;
    }

    public function updateFile($id_arquivo = null, $ordem = null, $nome = null)
    {
        if (empty($id_arquivo)) {
           throw new \Exception("ID do arquivo vazio.");
        }

        if (empty($ordem)) {
           $ordem = 0;
        }

        if (empty($nome)) {
           throw new \Exception("Nome vazio.");
        }

        $dados = array(
            'ordem' => $ordem,
            'nome' => $nome
        );

        if($this->getUploadModel()->update($dados, $id_arquivo)) {
            return true;
        }

        return false;
    }

    public function setUploadModel($model)
    {
        // Model não definido
        if(empty($model)) {
            throw new \Exception('O model não foi definido.');
        }

        // Cria o objeto do loader
        $this->setLoader();

        // Cria o objeto do model
        $this->oModel = $this->oLoader->getModel($model);
    }

    public function setParentId($id)
    {
        // Verifica se o código do informactive é válido
        if ($id == 0) {
            // Grava mensagem
            $msg = 'Código do informactive não encontrado';

            // Grava mensagem ao usuário
            $this->getController()->userMessenger()->addErrorMessage($msg);

            // Redireciona à lista de informactives
            return $this->getController()->redirect()->toRoute('informactive/user');
        }

        // Define o id
        $this->id = $id;
    }

    public function setLoader()
    {
        // Cria o loader
        $this->oLoader = new \Realejo\App\Loader\Loader();

        return $this->oLoader;
    }

    /**
     * Atalho para o getUploadAdapter() do model.
     *
     * @return Http
     */
    public function getUploadAdapter()
    {
        return $this->oModel->getUploadAdapter();
    }
}