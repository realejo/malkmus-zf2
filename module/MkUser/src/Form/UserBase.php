<?php
namespace MkUser\Form;

use ZfcUser\Form\Base as ZfcBase;

class UserBase extends ZfcBase
{
    public function __construct()
    {
        parent::__construct();

        // Remove the username
        $this->remove('username');

        // Remove the user id, it should come from the URL
        $this->remove('userId');

    }
}
