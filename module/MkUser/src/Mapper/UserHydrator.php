<?php

namespace MkUser\Mapper;

use ZfcUser\Mapper\UserHydrator as ZfcUserHydrator;

class UserHydrator extends ZfcUserHydrator
{
    protected function mapField($keyFrom, $keyTo, array $array)
    {
        if ($keyFrom === 'user_id') {
            $keyFrom = 'id_user';
        }
        if ($keyTo === 'user_id') {
            $keyTo = 'id_user';
        }
        if ($keyFrom === 'displayName') {
            $keyFrom = 'name';
        }
        if ($keyTo === 'name') {
            $keyTo = 'displayName';
        }
        return parent::mapField($keyFrom, $keyTo, $array);
    }
}
