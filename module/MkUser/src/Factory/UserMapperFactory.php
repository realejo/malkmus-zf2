<?php
namespace MkUser\Factory;

use Zend\Db;
use Zend\Crypt\Password\Bcrypt;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Stdlib\Hydrator;
use MkUser\Mapper;
use ZfcUser\Options;

class UserMapperFactory implements FactoryInterface
{
    /**
     * {@inheritDoc}
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /* @var $options Options\ModuleOptions */
        $options = $serviceLocator->get('mkuser_module_options');

        /* @var $dbAdapter Db\Adapter\Adapter */
        $dbAdapter = $serviceLocator->get('mkuser_zend_db_adapter');

        $mapper = new Mapper\User();
        $mapper->setDbAdapter($dbAdapter);

        $entityClass = $options->getUserEntityClass();

        /* @var $hydrator Hydrator\HydratorInterface */
        //$hydrator = $serviceLocator->get('zfcuser_user_hydrator');
        $crypto  = new Bcrypt;
        $crypto->setCost($options->getPasswordCost());
        $hydrator = new Mapper\UserHydrator($crypto);

        $mapper
            ->setEntityPrototype(new $entityClass)
            ->setHydrator($hydrator)
            ->setTableName($options->getTableName())
        ;

        return $mapper;
    }
}
