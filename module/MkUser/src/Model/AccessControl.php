<?php
/**
 * Controle de acesso as funcionalidades do portal
 *
 * @copyright  Copyright (c) 2014 Realejo (http://www.realejo.com.br)
 */
namespace MkUser\Model;

use Zend\Authentication\AuthenticationService;

class AccessControl
{
    /**
     * @var AuthenticationService
     */
    private $_service;

    /**
     * @var \Zend\Authentication\Storage\StorageInterface
     */
    private $_user;

    /**
     * Roles do usuário logado em cada módulo
     *
     * @var array
     */
    public $roles = array();

    /**
     * @var Acl
    */
    public $acl;

    function __construct()
    {
        // Carrega os dados do usuário
        if ($this->getAuthenticationService()->hasIdentity()) {
            // Verifica se existe um usuário
            $user = $this->getAuthenticationService()->getStorage()->read();
            $this->setUser($user);
        }

        // Carrega os roles do usuário
        // Não sei como vamos fazer e ver esses roles
        if ($this->hasIdentity()) {
            //$oTemp = new \Bffc\Usuario\Acl();
            //$this->roles = $oTemp->getRolesCache($this->getUser()->id_user);
        }
    }

    /**
     * Retorne AuthenticationService utilizado
     *
     * @return AuthenticationService
     */
    public function getAuthenticationService()
    {
        if (!isset($this->_service)) {
            $this->_service = new AuthenticationService();
        }
        return $this->_service;
    }

    /**
     * Retorna o valor do cookie que controla a sessão no PHP
     *
     * @return string
     */
    private function _getSessionCookie()
    {
        $config = new \Zend\Session\Config\SessionConfig();
        $cookieName = $config->getStorageOption('name');

        return (isset($_COOKIE[$cookieName])) ? $_COOKIE[$cookieName] : null;
    }

    /**
     * Retorna nome do arquivo de controle de sessão do usuário
     *
     * @param string $cookie
     *
     * @return string
     */
    private function _getSessionJson($cookie = null)
    {
        if (empty($cookie)) {
            $cookie = $this->_getSessionCookie();
        }

        return \MkUser\Model\User\Service::getSessionPath() . '/sess_'.$cookie.'.json';
    }

    /**
     * Retorna se está logado
     *
     * Também cria um json para gravar as informações de sessão se não existir.
     * @todo colocar as informações dentro do arquivo de sessão
     *
     * @return bool
     */
    public function hasIdentity()
    {
        // Verificação do controle de sessão
        if ( $this->getAuthenticationService()->hasIdentity() ) {
            $idu = $this->getUser()->id_user;
            $cookie = $this->_getSessionCookie();

            // Verifica se existe o cookie de controle de sessão
            if (!empty($cookie)) {

                $sessionControlFile = $this->_getSessionJson($cookie);

                // Verifica se o arquivo de controle de sessão já foi criado
                if (is_file($sessionControlFile)) {
                    return true;
                }

                // @todo Esse código foi comentado para o nova oferta
                // que estava sendo apagado quando é executado o
                // setUsuario novamente.

                // Recarrega o user com os dados do banco de dados
                // para não pegar o cache de permissões
                //$this->setUsuario($cdu);

                // Verifica se ele está active
                if ($this->getUser()->active == 1 ) {
                    // Grava as informações da sessão
                    $session = array(
                        'ID'              => $cookie,
                        'USERID'          => $idu,
                        'USERNAME'        => $this->getUser()->name,
                        'USEREMAIL'       => $this->getUser()->email,
                        'REMOTE_ADDR'     => $_SERVER['REMOTE_ADDR'],
                        'HTTP_USER_AGENT' => $_SERVER['HTTP_USER_AGENT'],
                        'SINCE'           => date('Y-m-d H:i:s')
                    );
                    $f = fopen($sessionControlFile, 'w');
                    fwrite($f, \Zend\Json\Json::encode($session));
                    fclose($f);

                    // Retorna que tem identidade
                    return true;

                }

                // Se o usuário não estiver active, força o logout
                $this->getAuthenticationService()->clearIdentity();
            }
        }

        // Retorna que não está logado
        return false;
    }

    /**
     * Retorna os dados do usuário gravada na sessão
     *
     * @return stdObject
     */
    public function getIdentity()
    {
        return $this->getAuthenticationService()->getIdentity();
    }

    /**
     * Remove a sessão do usuário
     *
     * @todo remover o controle de sessão (json)
     */
    public function clearIdentity()
    {
        // Verifica se há arquivo de controle de sessão e remove
        $sessionControlFile = $this->_getSessionJson($this->_getSessionCookie());
        if (is_file($sessionControlFile)) {
            unlink($sessionControlFile);
        }

        // Remove a sessão
        $this->getAuthenticationService()->clearIdentity();
    }

    /**
     * Grava o usuário na sessão
     *
     * @param int|array|object $user
     *
     * @return AccessControl
     */
    public function setUser($user = null)
    {
        // Verifica se tem um usuário especificado
        if ($user === null && $this->hasIdentity()) {
            $user = $this->getUser()->id_user;
        }

        // Verifica se deve carregar o usuário do banco de dados
        if (is_numeric($user)) {
            // Carrega os dados do usuário
            $oTemp = new \MkUser\Model\User\Service();
            $user = $oTemp->setUseCache(false)->fetchRow($user, false);
        }

        if (empty($user)) {
            throw new \Exception('Nenhum usuário defindo em AccessControl::setUsuario()');
        }

        // Verifica se é um array
        if (!is_array($user)) {
            $user = (array) $user;
        }

        // Remove campos desnecessários
        if (isset($user['password'])) {
            unset($user['password']);
        }

        // Transforma em objeto
        if (!is_object($user)) {
            $user = (object) $user;
        }

        // Grava no storage
        $this->getAuthenticationService()->getStorage()->write($user);

        // Grava na sessão ativa
        $this->_user = $user;

        // Retorna a cadeia
        return $this;
    }

    /**
     * Retorna o usuário da sessão
     *
     * @param boolean $useArray OPCIONAL retorna o usuário como array e não como stdClass
     *
     * @return \stdClass|array
     */
    public function getUser($useArray = false)
    {
        if ($useArray === true) {
            (array) $this->_user;
        } else {
            return $this->_user;
        }
    }

    /**
     * Retorna o id do usuário da sessão
     *
     * @return int
     */
    public function getIdUsuario()
    {
        if ($this->hasIdentity()) {
            return (int) $this->getUser()->id_user;
        }
        return null;
    }

    /**
     * Verifica se é o mesmo usuário da sessão
     *
     * @param int     $cdu      Código do usuário
     * @param boolean $useUser (OPCIONAL) Retorna TRUE se for um userstrador
     *
     * @return boolean
     */
    public function isSame($cdu, $useUser = false)
    {
        // Verifica se é o userz
        if ($useUser && $this->isRoot()) return true;

        // Verifica se é o usuário
        if ($cdu === $this->getUser()->id_user) return true;

        // Não é o mesmo
        return false;
    }

    /**
     * Retorna se o usuário é um superusuário. Ele é automaticamewnte useristrador de todos os módulos
     *
     * @param array   $user  (OPCIONAL) Valida um usuário ao invés do usuário logado
     *
     * @return bool
     */
    public function isRoot($user = null)
    {
        // Verifica se deve verificar a identidade
        $hasIdentity = (empty($user)) ? $this->hasIdentity() : true;

        // Verifica se foi passado um usuário
        if (empty($user)) {
            $user = $this->getUser();
        } elseif (is_array($user)) {
            $user = (object) $user;
        }

        return ($hasIdentity
                && $user->root == 1);
    }

    /**
     * Informa se o usuário é um funcionário da Realejo.
     *
     * @return bool
     */
    public function isRealejo()
    {
        return ($this->isRoot()
            && strpos($this->getUser()->email, '@realejo.com.br') !== false);
    }

    /**
     * Informa se o usuário pode acessar o beta
     *
     * @param boolean $useUser (OPCIONAL) Retorna TRUE se for um userstrador
     *
     * @return bool
     */
    public function isBeta($useUser = false)
    {
        // Retorna TRUE se for user
        if ($useUser && $this->isRoot()) return true;

        // Retorna se possui o role
        return ($this->hasIdentity()
            && ($this->getUser()->beta == 1
                || strpos($this->getUser()->email, '@realejo.com.br') !== false));
    }
}