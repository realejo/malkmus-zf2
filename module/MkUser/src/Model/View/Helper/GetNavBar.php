<?php
/**
 * Universidade BFFC (http://universidade.bffc.com.br)
 *
 * @copyright Copyright (c) 2013 Realejo (http://realejo.com.br)
 */
namespace MkUser\Model\View\Helper;

use Zend\View\Helper\AbstractHelper;

/**
 * View helper plugin to fetch the authenticated identity.
 */
class GetNavBar extends AbstractHelper
{
    /**
     * Nav Bar
     *
     */
    protected $navBar = array();

    /**
     * Retorna o Menu do site
     *
     * @return array
     */
    public function __invoke()
    {
        if (!isset($this->navBar) || empty($this->navBar)) {
            $configpath = APPLICATION_ROOT . '/config/autoload/user.php';
            if ( file_exists($configpath) ) {
                $config = require $configpath;
            } else {
                $config = array();
            }
            if (!empty($config) && isset($config['navbar'])) {
                $this->navbar = $config['navbar'];
            } else {
                $this->navbar = $config;
            }
        }
        return $this->navbar;
    }
}
