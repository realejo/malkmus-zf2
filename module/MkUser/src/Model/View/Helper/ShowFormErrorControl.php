<?php
/**
 * Universidade BFFC (http://universidade.bffc.com.br)
 *
 * @copyright Copyright (c) 2013 Realejo (http://realejo.com.br)
 */
namespace MkUser\Model\View\Helper;

use Zend\View\Helper\AbstractHelper;

/**
 * View helper plugin to fetch the authenticated identity.
 */
class ShowFormErrorControl extends AbstractHelper
{

	/**
	 * Mensagem de validação de campo
	 *
	 * @param string $tipo
	 * @param string $mensagem
	 */
	public function showError($mensagem = null, $campo = null)
	{

	    // Verifica se é um array com os erros
	    if (is_array($mensagem) && !is_null($campo)) {
	        if (array_key_exists($campo, $mensagem)) {
	            $mensagem = $mensagem[$campo];
	        } else {
	            $mensagem = false;
	        }
	    }

	    // Verifica se deve retornar a mensagem de erro
		if (!empty($mensagem)){
		    if (!empty($campo)) {
		        return "<label class=\"error text-danger\" for=\"$campo\" generated=\"true\">$mensagem</label>";
	        } else {
	            return "<label class=\"error text-danger\">$mensagem</label>";
            }
		}

		// retorna vazio
		return '';
	}
}
