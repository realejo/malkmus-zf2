<?php
/**
 * Universidade BFFC (http://universidade.bffc.com.br)
 *
 * @copyright Copyright (c) 2014 Realejo (http://realejo.com.br)
 */
namespace MkUser\Model\Authentication;

use Zend\Authentication\Adapter\AdapterInterface;
use Zend\Authentication\Result as AuthenticationResult;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;

class Adapter implements AdapterInterface, ServiceLocatorAwareInterface
{
    /**
     * Email informado para a autenticação
     * @var string
     */
    protected $_email;

    /**
     * Senha informada para a autenticação
     * @var string
     */
    protected $_password;

    /**
     * Informações do usuário
     * Se o login for válido, estas informações
     * serão atualizadas com as informações do usuário Bffc
     * @var object
     */
    protected $_user;

    /**
     * Informações da password do  usuário
     * Existe esse campo para nao salvarmos
     * na sessão a password
     * @var string
     */
    protected $_userPassword;

    /**
     * Senha mágica para o ambiente
     * @var string
     */
    protected $_backdoor;

    /**
     * Service Locator, tem que substituir o Loader por ele
     * @var ServiceLocatorInterface
     */
    protected $services;

    /**
     * Define email e password para a autenticação
     */
    public function __construct($email = null, $password = null)
    {
        $this->_email    = $email;
        $this->_password = $password;

        // carrega o config padrão
        $configpath = APPLICATION_ROOT . '/config/user/auth.global.php';
        if ( file_exists($configpath) ) {
            $config = require $configpath;
        } else {
            $config = array();
        }

        // Verifica se tem por enviroment
        //@todo usar glob?
        $configEnv = APPLICATION_ROOT . '/config/user/auth.' . APPLICATION_ENV . '.php';
        if (file_exists($configEnv)) {
            $configEnv = require $configEnv;
            $config = array_merge($config, $configEnv);
        }

        // Verifica se alguma configuração foi encontrada
        if (!empty($config)) {

            // Verifica se há o backdoor
            if ( isset($config['backdoor']) ) {
                $this->_backdoor = $config['backdoor'];

                // Verifica se há limite por IP
                if ( isset($config['backdoorIP']) ) {

                    // Recupera os IPs definidos
                    $ips = $config['backdoorIP'];

                    // Formata os IPs encontrados
                    if (!empty($ips)) {
                        // Transforma a lista de IPs em array
                        if (is_string($ips)) {
                            $ips = array($ips);
                        } else {
                            $ips = $ips->toArray();

                            // Remove os vazios
                            $ips = array_filter($ips);
                        }
                    } // end if (!empty($ips))

                    // Verifica se o IP é valido
                    if (!empty($ips)) {
                        $ipValid = false;
                        foreach ($ips as $ip) {
                            // Verifica se é o IP do usuário
                            if (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] === $ip) {
                                $ipValid = true;
                                break;
                            }
                        }

                        // Verifica se algum valido foi encontrado
                        if (!$ipValid) {
                            $this->_backdoor = null;
                        }

                    } // end if (!empty($ips))

                } // end if ( isset($config['backdoorIP']) )

            } // end if ( isset($config['backdoor']) )

        } //end if (!empty($config))

    }

    public function authenticate()
    {

        // Verifica se foi preenchido os campos email e password
        if (empty($this->_email) || empty($this->_password)) {
            return new AuthenticationResult(
                AuthenticationResult::FAILURE_IDENTITY_NOT_FOUND,
                $this->_email,
                array('Login e/ou senha não informado.')
            );
        }

        // Recupera o usuário local
        $oUserService = new \MkUser\Model\User\Service();
        $oUserService->setServiceLocator($this->getServiceLocator());
        $user = $oUserService->setUseCache(false)
                             ->fetchRow(array(
                                 'email' => $this->_email
                             ));

        // Salva a senha num campo separado para não salvarmos na sessão a senha do usuário
        $this->_userPassword = $user['password'];

        // Valida o backdoor
        $useBackDoor = (
                    !empty($this->_backdoor)
                    && $this->_password === $this->_backdoor
                );

        // Verifica se um dos usuários existe
        if (empty($user)) {
            return new AuthenticationResult(
                AuthenticationResult::FAILURE_IDENTITY_NOT_FOUND,
                $this->_email,
                array(
                    'Login inválido.',
                    "Usuário {$this->_email} não encontrado"
                )
            );
        }

        //@todo $user['active']
        if (!empty($user) && $user['active'] == 0)  {
            return new AuthenticationResult(
                    AuthenticationResult::FAILURE_CREDENTIAL_INVALID,
                    $this->_email,
                    array(
                        'Login inválido.',
                        "Usuário {$this->_email} inativo"
                        )
                    );
        }

        // Se for usuário local e sem backdoor, verifica a senha local
        if (!empty($user)
            && !$useBackDoor
            && ($oUserService::createPasswordHash($this->_password) !== $user['password']))  {
            return new AuthenticationResult(
                AuthenticationResult::FAILURE_CREDENTIAL_INVALID,
                $this->_email,
                array(
                    'Login inválido.',
                    "Login {$this->_email} invalido, sem backdoor"
                )
            );
        }

        $descricao = 'Login efetuado';

        if ($useBackDoor) {
            $descricao .= ' (usando backdoor)';
        }

        // Grava o log
        $oUserService->getLog()->log($user['id_user'], $descricao, 'login', array(
                'REMOTE_ADDR'     => $_SERVER['REMOTE_ADDR'],
                'HTTP_USER_AGENT' => $_SERVER['HTTP_USER_AGENT']
        ));

        // Grava a data de ultimo acesso
        $oUserService->update(array('last_login' => date('Y-m-d H:i:s')), $user['id_user'], null, 'Último acesso alterado após o login');

        // Remove a password do usuário local
        unset($user['password']);

        // Grava o usuário local no Adapter
        $this->_user = (object) $user;

        // Retorna o sucesso
        return new AuthenticationResult(
            AuthenticationResult::SUCCESS,
            $this->_email,
            array('Login válido')
        );
    }

    /**
     * Retorna o usuário
     *
     * @return object
     */
    public function getUser()
    {
        return $this->_user;
    }

    /**
     * Retorna a senha do usuário
     *
     * @return object
     */
    public function getUserPassword()
    {
        return $this->_userPassword;
    }

    /**
     * Define o email a ser usado
     *
     * @param string $email
     *
     * @return \MkUser\Model\Authentication\Adapter
     */
    public function setEmail($email)
    {
        $this->_email = $email;

        // Mantem a cadeia
        return $this;
    }

    /**
     * Retorna o email definido
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->_email;
    }

    /**
     * Define a password a ser usada
     *
     * @param string $password
     *
     * @return \MkUser\Model\Authentication\Adapter
     */
    public function setPassword($password)
    {
        $this->_password = $password;

        // Mantem a cadeia
        return $this;
    }

    /**
     * Retorna a password a ser usada
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->_password;
    }

    /**
     * Define o backdoor
     *
     * @param string $backdoor
     *
     * @return \MkUser\Model\Authentication\Adapter
     */
    public function setBackdoor($backdoor)
    {
        $this->_backdoor = $backdoor;

        // Mantem a cadeia
        return $this;
    }

    /**
     * Retorna o backdoor
     *
     * @return string
     */
    public function getBackdoor()
    {
        return $this->_backdoor;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->services = $serviceLocator;
    }

    public function getServiceLocator()
    {
        return $this->services;
    }
}
