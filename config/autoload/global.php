<?php
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */

return array(
    'db' => array(
        'driver'   => 'Mysqli',
        'hostname' => '127.0.0.1',
        'database' => 'wrong db name',
        'username' => 'wrong username',
        'password' => 'wrong password',
        'charset'  => 'utf8',
        'driver_options' => array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
        ),
        'options' => array(
            'buffer_results' => true,
        ),
    ),
    'access_whitelist' => array(
        'login', 'login/esqueci', 'login/redefinir'
    ),
    'service_manager' => array(
        'factories' => array(
            'Zend\Db\Adapter\Adapter' => 'Zend\Db\Adapter\AdapterServiceFactory',
        ),
    ),
    'session' => array(
        'regenerate_id' => true,
        'config' => array(
            'class' => 'Zend\Session\Config\SessionConfig',
            'options' => array(
                'name' => 'ubs',
                //Tem um algum problema sessão do PHP que não deixa ele alterar o arquivo da sessão em uma pasta compartilhada
                //@todo corrigir isto!
                'save_path' => APPLICATION_DATA . '/sessions',
            ),
        ),
        'storage' => 'Zend\Session\Storage\SessionArrayStorage',
        'validators' => array(
            array(
                'Zend\Session\Validator\RemoteAddr',
                'Zend\Session\Validator\HttpUserAgent',
            ),
        ),
    ),
);
