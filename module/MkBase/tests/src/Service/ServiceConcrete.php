<?php
namespace MkBaseTest\Service;

use MkBase\Service\ServiceAbstract;

class ServiceConcrete extends ServiceAbstract
{
    /**
     * @var string
     */
    protected $mapperClass = '\RealejoZf1Test\Mapper\MapperConcrete';
}
