<?php
/**
 * Model para gerenciar os acesso de usuários
 *
 * @copyright Copyright (c) 2014 Realejo Design Ltda. (http://www.realejo.com.br)
 */
namespace MkUser\Model\User;

use Realejo\App\Model\Base as AppModel;

class Acl extends AppModel
{
    protected $table = 'user_acl';
    protected $key   = 'fk_user';

    /**
     * Grava os roles de um usuário
     *
     * @todo verificar se o role existe para o módulo antes de gravar
     *
     * @param int   $idu   Código do usuário
     * @param array $roles permissões em cada módulo
     */
    public function setRoles($idu, $roles)
    {
        // Apaga as permissões do usuário
        $this->getTableGateway()->delete("fk_user=$idu");

        // Grava as novas permissões
        if (is_array($roles)) {
            foreach ($roles as $modulo => $role) {
                if (!empty($role)) {
                    $this->getTable()->insert(array(
                            'fk_user' => $idu,
                            'modulo'  => $modulo,
                            'role'    => $role
                    ));
                }
            }
        }

        // Limpa o cache
        $this->getCache()->flush();

        // Recria o arquivo com as permissões
        $this->setRolesCache($idu);
    }

    /**
     * Recupera os roles em cada modulo
     *
     * @param int $idu código do usuário
     *
     * @return array Retorna um array com os roles ou null se nenhum for encontrado
     */
    public function getRoles($idu)
    {
        // Verifica se é o código do usuário
        if (!is_numeric($idu)) {
            throw new \Exception ("Código do usuário $idu não válido em MkUser\Model\Usuario\Acl");
        }

        // Recupera os roles
        $fetch = $this->fetchAll(array("fk_user"=>$idu));

        // Verifica se localizou algom role
        if (empty($fetch)) {
            return null;
        } else {
            return $fetch->toArray();
        }
    }

    /**
     * Pegar as permissões de um modulo do user
     */

    /**
     * Recupera o Role de um usuário em um determinado módulo
     *
     * @param int    $idu    Código do usuário
     * @param string $module Nome do módulo
     *
     * @return string|null   Role do usuário ou null se não possuir
     *
     * @todo Colocar no cache
     */
    public function getRole($idu, $module)
    {
        // Configura as condições
        $where = array('fk_user'=>$idu, 'modulo'=>$module);

        // Recupera o role
        $role = $this->fetchRow($where);

        // Retorna role
        return (count($role)>0) ? $role['role'] : null;
    }

    /**
     * Grava o cache das roles do usuário
     *
     * @param int $idu código do usuário
     */
    public function setRolesCache($idu)
    {
        // Recupera as roles
        $roles = $this->getRoles($idu);

        // Associa pelo nome do modulo
        $cache = array();
        if (!empty($roles)) {
            foreach ($roles as $r) {
                if (!empty($r['role'])) {
                    $cache[$r['modulo']] = $r['role'];
                }
            }
        }

        // Define o nome do arquivo
        $cacheFile = $this->getRolesCachePath($idu);

        // Grava o cache
        $f = fopen($cacheFile, 'w');
        fwrite($f,'<?php return ' . var_export($cache, true) .';');
        fclose($f);
    }

    /**
     * Recupera o cache dos roles do usuário
     *
     * @param int $idu código do usuário
     */
    public function getRolesCache($idu)
    {
        // Define o nome do arquivo
        $cachefile = $this->getRolesCachePath($idu);

        // Verifica se o arquivo existe
        if (!file_exists($cachefile)) $this->setRolesCache($idu);

        // retorna as roles
        return require $cachefile;
    }

    static public function getRolesCachePath($idu = null)
    {
        $path = realpath(APPLICATION_DATA.'/acl/roles');

        // Define a pasta de cache
        $path = APPLICATION_DATA . '/acl/roles';

        // Verifica se a pasta do cache existe
        if (!file_exists($path)) {
            $oldumask = umask(0);
            mkdir($path, 0777, true); // or even 01777 so you get the sticky bit set
            umask($oldumask);
        }

        if ($idu !== null) $path .= "/$idu.php";

        // Retorna o arquivo de cache de permissões
        return $path;
    }
}